class TranscriptSampler:

    def all_in_one(file, number, n_file):
        dictionary = {}
        print(1)
        my_file = open(file, 'r')

        for line in my_file:
            stripped_line = line.strip()
            words = stripped_line.split()
            gene_id = words[0]
            nr_copies = words[1]

            dictionary[gene_id] = nr_copies

            print(2)
        print(3)

        total_abundance = 0
        for k in dictionary:
            total_abundance += int(dictionary[k])

        sample = {}
        relative_abundance = 0
        for k in dictionary:
            relative_abundance = int(dictionary[k]) / total_abundance
            number_to_express = int(relative_abundance) * int(number)
            sample[k] = str(round(number_to_express))

        new_file = open(n_file, 'w')

        for k in (sample.keys()):
            my_tupple = k, sample[k]
            first_string = my_tupple[0]
            second_string = my_tupple[1]
            new_file.write(first_string + ' ' + second_string + '\n')

        new_file.close()
