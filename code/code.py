#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def read_avg_expression(file):
    dictionary = {}
    print(1)
    my_file = open(file,'r')
    
    for line in my_file:
        stripped_line = line.strip()
        words = stripped_line.split()
        gene_id = words[0]
        nr_copies = words[1]
        
        dictionary[gene_id] = nr_copies

        print(2)
    print(3)
    return(dictionary)


# In[ ]:


read_avg_expression('test.txt')


# In[ ]:


def sample_transcripts(avgs, number):
    
    total_abundance = 0
    for k in dictionary:
        total_abundance += int(dictionary[k])
    
    sample = {}
    relative_abundance = 0
    for k in dictionary:
        relative_abundance = int(dictionary[k]) / total_abundance
        number_to_express = relative_abundance * number
        sample[k] = str(round(number_to_express))
    return sample


# In[ ]:


sample_transcripts(0, 15)


# In[ ]:


def write_sample(file, sample):
    new_file = open(file,'w')
    
    for k in (sample.keys()):
        my_tupple = k, sample[k]
        first_string = my_tupple[0]
        second_string = my_tupple[1]
        new_file.write(first_string + ' ' + second_string + '\n')

    new_file.close()


# In[ ]:


write_sample('tests.txt', sample)

