if __name__ == '__main__':

    import transcript_sampler
    import argparse

    parser = argparse.ArgumentParser(description='Do something')
    parser.add_argument('file', metavar='FILE', type=str, help='Enter the path of the input file')
    parser.add_argument('number', metavar='NUMBER', type=str, help='Enter the total number of expression')
    parser.add_argument('n_file', metavar='NEW FILE', type=str, help='Enter the path of the output file')

    args = parser.parse_args()

    file = args.file
    number = args.number
    n_file = args.n_file

    transcript_sampler.TranscriptSampler.all_in_one(file, number, n_file)
