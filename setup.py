from setuptools import setup, find_packages

setup(
    name='valgal_first',
    url='https://gitlab.com/valentingallier/first-git',
    author='Valentin Gallier',
    author_email='valentin.gallier@stud.unibas.ch',
    description='First test package, programming for life sciences lecture',
    license='MIT',
    version='1.0.0',
    packages=find_packages(),  # this will autodetect Python packages from the directory tree, e.g., in `code/`
    install_requires=['argparse'] # add here packages that are required for your package to run, including version or range of versions
    entry_points={'console_scripts': ['sample_as=valgal_first.cli:main']}
    )